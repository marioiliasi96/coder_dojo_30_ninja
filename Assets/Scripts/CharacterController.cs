﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : Controller
{
    //rigidbody declaration
    private Rigidbody2D rigidbody2D;
    //force that pushes rigidbody
    public float jumpForce = 100f;
    Animator animator;
    bool facingRight = true;
    private bool canJump;
    private float lastJump;
    private bool attack;
    // Use this for initialization

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {

            if (Time.time - lastJump > 0.5F)
            {
                canJump = true;
                lastJump = Time.time;
            }
        } else if(attack && collision.gameObject.tag == "EnemyRobot")
        {
            GameObject enemy = collision.gameObject;
            HealthBar enemyHealthBar = enemy.GetComponent<HealthBar>();
            if (enemyHealthBar.health > 0)
            {
                enemyHealthBar.health -= 10F;
            }
            Debug.Log(enemyHealthBar.health);
        } else if(attack && collision.gameObject.tag.Equals("Bomb"))
        {
            GameObject go = collision.gameObject;
            go.GetComponent<Explosion>().explode();
        }
    }

    void Start ()
    {
        animator = GetComponent<Animator>();
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        attack = Input.GetMouseButtonDown(0);
        if (attack == true && canJump == true)
        {
            //rigidbody2D.AddForce(Vector2.up * jumpForce);
            animator.SetTrigger("attack");
        }
        else
        {
            float horizontal = Input.GetAxis("Horizontal");
            animator.SetFloat("speed", Mathf.Abs(horizontal));
            transform.Translate(speed * horizontal * Time.deltaTime, 0, 0);
            flip(horizontal);
        }
    }

    private void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.Space) && canJump)
        {
            canJump = false;
            rigidbody2D.AddForce(Vector2.up * jumpForce);
            animator.SetTrigger("jump");
            Debug.Log("bla");
        }
    }

    void flip(float horizontal)
    {
        if( (horizontal > 0 && facingRight == false) || (horizontal < 0 && facingRight))
        {
            facingRight = !facingRight;
            SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
            spriteRenderer.flipX = !facingRight;
        }
    }
}
