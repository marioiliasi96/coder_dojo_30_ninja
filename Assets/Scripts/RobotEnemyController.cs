﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotEnemyController : Controller
{
    private CapsuleCollider2D col;
    private Rigidbody2D rb;
    private HealthBar healthBar;
    // Use this for initialization
    void Start()
    {
        col = GetComponent<CapsuleCollider2D>();
        rb = GetComponent<Rigidbody2D>();
        healthBar = GetComponent<HealthBar>();
    }
    
    // Update is called once per frame
    void Update()
    {
        if (healthBar.health == 0)
        {
            if (rb.simulated) {
                //col.enabled = false;
                rb.simulated = false;
            }
        }
    }
}
