﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

    public float health { get; set; }
    private Vector3 color { get; set; }
    public GameObject healthBar;
    private Vector3 scale;
    private Animator animator;

	// Use this for initialization
	void Start () {
        health = 100f;
        color = new Vector3(0, 255, 0);
        scale = healthBar.transform.localScale;
        animator = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (health <= 0)
        {
            animator.SetTrigger("dead");
        } else if (health < 25)
        {
            color = new Vector3(255, 0, 0);
        }
        else if(health < 50)
        {
            color = new Vector3(255, 230, 0);
        } else if(health < 75)
        {
            color = new Vector3(200, 255, 0);
        }
        healthBar.transform.localScale = new Vector3(health/100 * scale.x, scale.y, scale.z);
        SpriteRenderer img = healthBar.GetComponent<SpriteRenderer>();
        img.color = new Color(color.x, color.y, color.z, 1);

    }
}
